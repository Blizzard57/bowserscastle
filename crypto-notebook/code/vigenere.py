key = 'mykey'
message = 'plztransferthis'

i=0
while len(key) <= len(message):
    key += key[i]
    i+=1

ciphertext = ''
for i,_ in enumerate(message):
    ciphertext += chr((ord(message[i]) + ord(key[i]) 
                    - 2 * ord('a')) % 26 
                    + ord('a'))
                
print(ciphertext)