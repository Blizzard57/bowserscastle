import random

# A predefined hash table (Key)
key_table = random.sample(range(26),26)
n = 3 #No of Rotors
m = 'known_message_here'

def rotor_speed(n):
    '''
    The function that decides the speed of each rotor
    '''
    return int(n)

c = ''
for i,_ in enumerate(m):
    c_i = m[i] # Temp location to store ith value
    for j in range(n):
        c_i = chr(key_table[(ord(c_i) 
            - ord('a') + rotor_speed(j)*i)
            %26] + ord('a'))
    c += c_i

print(c) # Cipher