import random

# A predefined hash table (Key)
key_table = random.sample(range(26),26)

m = 'known_message_here'

c = ''
for i in m:
    c += chr(key_table[ord(i) 
            - ord('a')] + ord('a'))

print(c) # Cipher