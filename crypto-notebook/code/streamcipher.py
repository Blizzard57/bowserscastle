import random

def xor(a : str,b : str) -> str:
    ''' 
    The length of the output string mst be equal 
    as the input ones, which cannot be ensured 
    in the default method. Hence, the function 
    is defined. 
    '''
    if len(a) != len(b):
        return ValueError('''The length of both the 
                        bitstrings must be equal''')

    val = ''
    for i,_ in enumerate(a):
        if a[i] == '0' and b[i] == '0' or a[i] == '1' and b[i] == '1':
            val += '0'
        else:
            val += '1'
    return val

def bin2dec(a : str) -> int:
    val = 0
    length = len(a)
    for i,bit in enumerate(a):
        val += int(bit) * (2**(length - i - 1))
    return val

k = '0101001'
m = '010010101110110110111000'

random.seed(k)
g_k = int(random.uniform(10**len(m),10**(len(m)+1)))

print(g_k)