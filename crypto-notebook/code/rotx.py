X = 18 # Rot 13 is the most popular
m = 'BO20{xtmiam_lwvb_bzg_apqnba_tqsm_bpqa_ib_pwum}'

c = ''
for i in m:
    if i >= 'a' and i<='z':
        c += chr((ord(i) - ord('a')
                + X) % 26 + ord('a'))
    
    elif i>= 'A' and i <= 'Z':
        c += chr((ord(i) - ord('A')
                + X) % 26 + ord('A'))
    else :
        c += i

print(c) # Cipher