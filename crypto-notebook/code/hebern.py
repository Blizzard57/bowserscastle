import random

# A predefined hash table (Key)
key_table = random.sample(range(26),26)

m = 'known_message_here_which_is_longer_for_decryption'

c = ''
for i,_ in enumerate(m):
    c += chr(key_table[(ord(m[i]) 
            - ord('a') + i)%26] + ord('a'))

print(c) # Cipher