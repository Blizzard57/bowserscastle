c = '''qnaoyphgidqyagrzsraldpmcvs
        houyygqmxnmtqahyslsbtkk''' # This is small

cipher_table = [[],[],[],[],[],[],[],[],[],[],
                [],[],[],[],[],[],[],[],[],[],
                [],[],[],[],[],[]]

for i,_ in enumerate(c):
    cipher_table[i%26].append(c[i])

key_table = []
for i in range(26):
    key_table.append(substitution_solver
                    (cipher_table[i]))

# Assuming that the key table gotten is correct

# Inverting : A -> E ==> E -> A
# Hence inverting the table
decrypt_key_table = invert(key_table)

# Now applying Hebern on cipher text
# with inverse_key_table
m = hebern(c,decrypt_key_table)

print(m) #Message