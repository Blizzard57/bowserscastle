\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}Overview}{5}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Uses}{5}{subsection.1.1.1}%
\contentsline {chapter}{\numberline {2}Information Theory}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Information}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Security}{8}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Shannon Theorem}{8}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Randomness}{9}{subsection.2.2.2}%
\contentsline {chapter}{\numberline {3}Probability Theory}{10}{chapter.3}%
\contentsline {chapter}{\numberline {4}Historical Ciphers}{11}{chapter.4}%
\contentsline {section}{\numberline {4.1}Substitution Cipher}{11}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Code}{11}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Rot - X}{12}{subsection.4.1.2}%
\contentsline {subsubsection}{Code}{12}{section*.3}%
\contentsline {subsection}{\numberline {4.1.3}Caesar Cipher}{12}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Breaking the Cipher}{13}{subsection.4.1.4}%
\contentsline {subsubsection}{Code}{14}{section*.4}%
\contentsline {subsubsection}{Working}{15}{section*.5}%
\contentsline {section}{\numberline {4.2}Vigen\IeC {\`e}re Cipher}{16}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Code}{16}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Breaking the Cipher}{17}{subsection.4.2.2}%
\contentsline {subsubsection}{Code}{17}{section*.6}%
\contentsline {section}{\numberline {4.3}Rotor Machines}{19}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Code}{20}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Enigma Machine}{21}{subsection.4.3.2}%
\contentsline {subsubsection}{Code}{21}{section*.7}%
\contentsline {subsection}{\numberline {4.3.3}Breaking the Cipher}{21}{subsection.4.3.3}%
\contentsline {subsubsection}{Code}{22}{section*.8}%
\contentsline {section}{\numberline {4.4}Conclusion}{22}{section.4.4}%
\contentsline {chapter}{\numberline {5}Symmetric Ciphers}{23}{chapter.5}%
\contentsline {section}{\numberline {5.1}One Time Pad}{24}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Code}{25}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Pros and Cons}{25}{subsection.5.1.2}%
\contentsline {subsubsection}{Pros}{25}{section*.9}%
\contentsline {subsubsection}{Cons}{26}{section*.10}%
\contentsline {subsection}{\numberline {5.1.3}Perfect Secrecy}{26}{subsection.5.1.3}%
\contentsline {section}{\numberline {5.2}Stream Cipher}{27}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Code}{27}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Conditions}{27}{subsection.5.2.2}%
