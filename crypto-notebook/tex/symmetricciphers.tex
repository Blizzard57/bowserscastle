\chapter{Symmetric Ciphers}
In this chapters, symmetric ciphers will be explored, which in 
general terms represent this :\\

\begin{definition}
    tThe definition of Symmetric Ciphers is as follows :\\\\
    \indent$ E : \mathcal{K},\mathcal{M} \rightarrow \mathcal{C}$,\\
    \indent$ D : \mathcal{K},\mathcal{C} \rightarrow \mathcal{M}$,\\\\
    \indent st, $\forall m \in \mathcal{M}, k \in \mathcal{K}$\\
    \indent$ D(k,E(k,m)) = m$
\end{definition}

\noindent From the definition above it is clear that $D(k,E(k,m)) = m$ is 
a required condition for the cipher to be defined as a symmetric 
cipher.
\\\\
There are also some conditions which $E,D$ satisfy, which are not requied 
but are highly recommended. $D$ always has to be deterministic whereas $E$ 
is often randomized.
\\\\
This means that $E(k,m)$ can give different outputs every time it is run, but 
$D(k,c)$ has to give the same output every time.
\\\\
This is to do with the fact that a ciphertext is illegible and on the face value 
does not contain any information. Hence, the more random it is, greater the 
security. But the message is the information that is to be conveyed. It cannot 
be random and has to be completely deterministic.
\\\\
Hence, for any encryption scheme to defined as a symmetric cipher, a theorem 
is given as follows :

\begin{theorem}
    tThe theorem is as follows :\\\\
    \indent$\forall k \in \mathcal{K}, m \in \mathcal{M}$\\
    \indent$D(k,E(k,m)) = m \Leftrightarrow$ Symmetric Cipher
\end{theorem}

\section{One Time Pad}
A One Time Pad is a secure encryption scheme, that was developed by 
Vernam in 1917. It is a symmetric cipher defined in the following way.

\begin{definition}
    aA One Time Pad is defined as :\\\\
    \indent$E(k,m) = k \oplus m$\\
    \indent$D(k,c) = k \oplus c$\\\\
    \indent where,\\
    \indent$\mathcal{K} = \mathcal{M} = \mathcal{C} = \{0,1\}^n$\\
    \indent$|k| = |m| $
\end{definition}

\noindent So, it can be seen that, the key length must be equal to the 
message length. It can also be seen that all of $k,m,c$ are bit strings, 
thus the operation of $\oplus$ (XOR) is defined on it.

\begin{example}
    tThis is an example of a One Time Pad :\\\\
    \indent Let m = 01111001 and k = 10100100\\
    \indent $\therefore$ $c = k\oplus m$ \& $m = k\oplus c$
    \begin{verbatim}
        k = 10100100               k = 10100100
        m = 01111001               c = 11011101
        ------------ XOR           ------------ XOR
        c = 11011101               m = 01111001
    \end{verbatim}
    \noindent This also satisfies the symmetric requirements
\end{example}

\subsection{Code}
This is the code for a One Time Pad encryption scheme :\\

\lstinputlisting[basicstyle=Large,style=py]{code/otp.py}

\subsection{Pros and Cons}
The One Time Pad (OTP) is a good encryption scheme, but has its share 
of pros and cons, making it unusable as a stand alone encryption system 
to secure a communication.
\\\\
\subsubsection{Pros}
Here are benefits of a One Time Pad encryption scheme.
\begin{itemize}[{\checkmark}]
    \item Extremely fast encryption and decryption
    \begin{itemize}[]
        \item It requires a single pass over the message (or key). Hence,
              the time required for encryption is $O(n)$, which is 
              extremely fast for an encryption mechanism. Decryption is 
              also the same mathematical function and hence requires 
              the same amount of time. 
    \end{itemize}
\end{itemize}

\noindent This is the only 'big' benefit of the OTP encryption scheme.

\subsubsection{Cons}
There are many cons in this scheme making it practically unusable.

\begin{itemize}[{$\times$}]
    \item Key is as long as the message
    \begin{itemize}[]
        \item The key being the same length as that of the message 
              is a big issue. Before the communication Alice has to 
              send Bob the key, which itself requires a secure channel 
              to transfer. If we can share the key, we might as well 
              share the message itself.
    \end{itemize}
    \item Key can be found out easily
    \begin{itemize}[]
        \item If by some method, the attacker comes across the message 
              and the cipher, he can easily find the key by $k=m\oplus c$.
              This makes the key unusable a second time. Thus the name 
              One Time Pad.
    \end{itemize}
\end{itemize}

\subsection{Perfect Secrecy}
As defined in Chapter 2, a cipher with perfect secrecy is the one 
that cannot be broken with a cipher only attack.
\\\\
\begin{lemma}
    oOne Time Pad (OTP) has Perfect Secrecy
\end{lemma}

\begin{proof}
    tTo prove :\indent$P(E(m,k) = c) = constant$\\
    Proof :\\\\
    \indent$\forall m \in \mathcal{M}, c \in \mathcal{C}$\\
    \indent$P(E(m,k) = c) = \frac{|\{k \in \mathcal{K}\ : \ E(m,k) = c\}|}{|\mathcal{K}|}$\\\\
    \indent So, $\forall m,c$\\
    \indent Let $S = \{k \in \mathcal{K} : E(m,k) = c\}$ \\\\
    \indent $E(m,k) \equiv k \oplus m$\\
    \indent $\implies k \oplus m = c$\\
    \indent $\implies k = c \oplus m$\\
    \indent $\implies |S| = 1$\\\\
    \indent Then, $|S| = constant$\\
    \indent And $|\mathcal{K}| = constant$\\\\
    \indent $\therefore P(E(m_0,k) = c) = \frac{const_1}{const_2}$
    $\implies P(E(m_0,k) = c) = constant$
\end{proof}

\section{Stream Cipher}
Stream Cipher is a version of One Time Pass (OTP) which 
is actually usable. The concept is to not have the key 
remain completely random but rather pseudo random.
\\\\
Hence the key $k$ is selected from a much smaller key space 
$\mathcal{K}$ and PRG $g$ is applied to it to give a key $g(k)$ with 
a $|g(k)| >> |k|$.

\begin{definition}
    aA stream cipher is a cipher with :\\\\
    \indent $E(m,k) = m \oplus g(k)$\\
    \indent $D(c,k) = c \oplus g(k)$
\end{definition}

\noindent $|k| << |m|$, hence the cipher is not perfectly secret, 
but as already stated earlier, no usable cipher will maintain 
perfect secrecy.

\subsection{Code}
This is the code for a stream cipher :

\subsection{Conditions}
There are certain steps that must be taken, to make the cipher secure.