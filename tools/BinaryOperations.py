'''
The code below provides class to deal with binary manipulations like conversions from Hex to Binary and 
ASCII and other such conversions in a simple manner.
'''

class BinaryOperations():
    def __init__(self,x):
        self.x = x

    def to_binary(self):
        if type(self.x) == int:
            return bin(self.x)

if __name__ == "__main__":
    s =  BinaryOperations(14)
    print(s.to_binary())