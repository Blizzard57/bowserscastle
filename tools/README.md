# Tools

These are tools which will be used to explain the most common attacks and have codes to solve them (or links to websites which solve it). The point of these tools are to eliminate need to solve the most basic of questions and get answers faster.

## Format

The folder structure should be as follows :

    Tool Name
        |- Method.md (Explains the theory behind the exploit)
        |- solve.py
            |- class tool_name (Minimum amount of methods)
                |- init
                |- solve
                |- get_key
        |- Usability.md (Explains how to use the tool)
        |- Example
            |- problem
            |- solution.py (this calls the class defined before)